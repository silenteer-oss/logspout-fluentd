module github.com/dsouzajude/logspout-fluentd

go 1.15

require (
	github.com/bmizerany/assert v0.0.0-20160611221934-b7ed37b82869 // indirect
	github.com/fluent/fluent-logger-golang v1.4.0
	github.com/fsouza/go-dockerclient v1.4.1
	github.com/gliderlabs/logspout v3.2.6+incompatible
	github.com/kr/pretty v0.1.0 // indirect
	github.com/philhofer/fwd v1.0.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.2.2
	github.com/tinylib/msgp v1.1.0 // indirect
)
