#!/bin/sh

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd ${CURRENT_DIR}


HOST="registry.gitlab.com/silenteer-oss/logspout-fluentd"
VERSION='1.0'

docker build -t ${HOST}:${VERSION} -f Dockerfile .
docker push ${HOST}:${VERSION}



